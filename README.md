# Neural Networks and Deep Learning

 
## Course Overview
You will get practical knowledge to perform general machine learning and, in particular, computer vision tasks with TensorFlow 2 and the neccessary theoretical background to troubleshoot when transferring the knowledge to solve own problems.

## Get Started

1) Start the jupyter lab server and enter your credentials. (If you want to run your own jupyter notebook on your computer, make sure you have installed the conda packages listed below)

2) Navigate to the exercises in the  **exercises/** folder

3) Open the notebook of choice and follow the instructions


## Used conda packages:
* jupyter
* tensorflow==2.1.0
* scikit-learn
* matplotlib
* opencv
* shap  (conda-forge -> 'conda install -c conda-forge shap')


## Schedule
* Before the course: Get to know jupyter, python and numpy with exercise 0

* **9:00-10:15**: Lecture on neural network basics (Speaker: Roger Labahn)
* 10:15-10:30: Break
* 10:30-12:30: Machine Learning Exercises. Support from Daniel Böckenhoff and Andrea Merlo via ZOOM 
    * 10:30-10:45: Introduction to neural network exercises (Speaker: Daniel Böckenhoff)
    * 10:45-11:15: Exercise 1: "Fully Connected Neural Network for hand written digit classification"
    * 10:15-11:45: Advanced Building Blocks for Neural Networks - Convolution, Inception, Pooling (Speaker: Daniel Böckenhoff)
    * 11:45-12:30: Exercise 2: "Convolutional Neural Network for hand written digit classification", optional additionally further exercises marked with "optional_...". Choice is up to your interest.
* 12:30-13:30 Lunch break
* 13:30-14:00: Wrap up / Discussion of exercises (Speaker: Daniel Böckenhoff)
* **14:00-15:00**: Presentation on artificial neural networks in a fusion/stellarator context (45+15) (Speaker: Daniel Böckenhoff)
